module org.stcharles.diapason {
    requires javafx.controls;
    requires javafx.fxml;
    requires kotlin.stdlib;

    exports org.stcharles.diapason.domain.service;
    exports org.stcharles.diapason.domain.model;
    exports org.stcharles.diapason.domain.repository;
    exports org.stcharles.diapason;
    exports org.stcharles.diapason.presentation;

    opens org.stcharles.diapason to javafx.fxml;
    opens org.stcharles.diapason.presentation to javafx.fxml;
}