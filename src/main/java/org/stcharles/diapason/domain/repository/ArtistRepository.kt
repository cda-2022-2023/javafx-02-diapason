package org.stcharles.diapason.domain.repository

import org.stcharles.diapason.domain.model.Artist
import java.util.*

interface ArtistRepository {
    fun findAll(): List<Artist>
    fun find(id: UUID): Artist?
}