package org.stcharles.diapason.domain.repository

import org.stcharles.diapason.domain.model.Album
import org.stcharles.diapason.domain.model.Artist

interface AlbumRepository {
    fun findAll(): List<Album>
    fun findByArtist(artist: Artist): List<Album>
}