package org.stcharles.diapason.domain.service.exception

class NoSuchArtistException : Throwable()