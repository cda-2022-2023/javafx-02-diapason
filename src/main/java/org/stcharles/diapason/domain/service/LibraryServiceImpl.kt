package org.stcharles.diapason.domain.service

import org.stcharles.diapason.domain.model.Album
import org.stcharles.diapason.domain.model.Artist
import org.stcharles.diapason.domain.repository.AlbumRepository
import org.stcharles.diapason.domain.repository.ArtistRepository
import org.stcharles.diapason.domain.service.exception.NoSuchArtistException
import java.util.*

class LibraryServiceImpl(private val artistRepository: ArtistRepository, private val albumRepository: AlbumRepository) :
    LibraryService {
    override fun findAllArtists() = artistRepository.findAll()
    override fun findArtist(id: UUID): Artist? = artistRepository.find(id)

    override fun findAllAlbums() = albumRepository.findAll()
    override fun findAlbumsByArtist(artistId: UUID): List<Album> {
        val artist = artistRepository
            .find(artistId) ?: throw NoSuchArtistException()

        return albumRepository
            .findByArtist(artist)
    }
}