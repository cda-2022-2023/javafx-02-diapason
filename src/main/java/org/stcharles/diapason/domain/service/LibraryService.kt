package org.stcharles.diapason.domain.service

import org.stcharles.diapason.domain.model.Album
import org.stcharles.diapason.domain.model.Artist
import org.stcharles.diapason.domain.service.exception.NoSuchArtistException
import java.util.*

interface LibraryService {
    fun findAllArtists(): List<Artist>
    fun findArtist(id: UUID): Artist?

    fun findAllAlbums(): List<Album>
    fun findAlbumsByArtist(artistId: UUID): List<Album>
}