package org.stcharles.diapason.domain.model

import java.util.*


class Artist(val name: String) {
    val id: UUID = UUID.randomUUID()
}