package org.stcharles.diapason.domain.model

import kotlin.time.Duration

class Song(val title: String, val duration: Duration)