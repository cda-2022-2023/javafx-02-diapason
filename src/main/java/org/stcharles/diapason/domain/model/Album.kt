package org.stcharles.diapason.domain.model

import java.util.*
import kotlin.time.Duration

class Album(artist: Artist, val title: String, val year: Int, songs: List<Song>) {
    val id: UUID = UUID.randomUUID()
    val artistId: UUID
    val songs: List<Song>

    init {
        this.artistId = artist.id
        this.songs = songs.toList()
    }

    val duration: Duration
        get() = songs
            .fold(Duration.ZERO) { acc, song -> acc.plus(song.duration) }
}