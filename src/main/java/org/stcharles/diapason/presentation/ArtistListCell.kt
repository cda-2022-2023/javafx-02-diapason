package org.stcharles.diapason.presentation

import javafx.scene.control.ListCell
import org.stcharles.diapason.domain.model.Artist

class ArtistListCell : ListCell<Artist?>() {
    override fun updateItem(item: Artist?, empty: Boolean) {
        super.updateItem(item, empty)
        text = if (empty) null else {
            item?.name
        }
    }
}