package org.stcharles.diapason.presentation

import javafx.fxml.FXML
import javafx.scene.control.ListView
import org.stcharles.diapason.domain.model.Artist
import org.stcharles.diapason.domain.service.LibraryService

class MainScreenController(private val libraryService: LibraryService) {
    @FXML
    private lateinit var artistsListView: ListView<Artist>

    @FXML
    private fun initialize() {
        artistsListView.setCellFactory { ArtistListCell() }
        artistsListView.items.setAll(libraryService.findAllArtists())
    }
}