package org.stcharles.diapason

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.stage.Stage
import javafx.util.Callback
import org.stcharles.diapason.domain.service.LibraryServiceImpl
import org.stcharles.diapason.infrastructure.repository.AlbumRepositoryInMemoryImpl
import org.stcharles.diapason.infrastructure.repository.ArtistRepositoryFileSystemImpl
import org.stcharles.diapason.presentation.MainScreenController

fun main(args: Array<String>) {
    Application.launch(HelloApplication::class.java, *args)
}

class HelloApplication : Application() {
    override fun start(stage: Stage) {
        // Chargement du template
        val fxmlLoader = FXMLLoader(HelloApplication::class.java.getResource("hello-view.fxml"))

        // Injection de dépendances
        fxmlLoader.controllerFactory = Callback {
            MainScreenController(
                LibraryServiceImpl(
                    ArtistRepositoryFileSystemImpl(),
                    AlbumRepositoryInMemoryImpl(),
                )
            )
        }

        val scene = Scene(fxmlLoader.load(), 320.0, 240.0)
        stage.title = "Diapason"
        stage.scene = scene
        stage.show()
    }
}