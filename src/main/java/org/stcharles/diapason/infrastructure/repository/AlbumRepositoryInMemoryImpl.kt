package org.stcharles.diapason.infrastructure.repository

import org.stcharles.diapason.domain.model.Artist
import org.stcharles.diapason.domain.repository.AlbumRepository

class AlbumRepositoryInMemoryImpl : AlbumRepository {
    override fun findAll() = InMemoryDataSource.albums

    override fun findByArtist(artist: Artist) = InMemoryDataSource
            .albums
            .filter { it.id == artist.id }
}