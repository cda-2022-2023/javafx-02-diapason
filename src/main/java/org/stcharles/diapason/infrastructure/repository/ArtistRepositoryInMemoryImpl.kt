package org.stcharles.diapason.infrastructure.repository

import org.stcharles.diapason.domain.repository.ArtistRepository
import java.util.*

class ArtistRepositoryInMemoryImpl : ArtistRepository {
    override fun findAll() = InMemoryDataSource.artists

    override fun find(id: UUID) = InMemoryDataSource
            .artists
            .find { it.id == id }
}