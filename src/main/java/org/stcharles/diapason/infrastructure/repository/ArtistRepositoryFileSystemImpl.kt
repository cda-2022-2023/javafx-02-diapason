package org.stcharles.diapason.infrastructure.repository

import org.stcharles.diapason.domain.model.Artist
import org.stcharles.diapason.domain.repository.ArtistRepository
import java.io.File
import java.util.*
import kotlin.io.path.Path

class ArtistRepositoryFileSystemImpl : ArtistRepository {
    private val artists: List<Artist>

    init {
        this.artists = Path("/home/thomas/Musique")
            // Accès au dossier
            .toFile()
            // Exploration récursive de haut en bas, renvoie un itérable
            .walkTopDown()
            // On ne retient que les fichiers MP3
            .filter { it.extension == "mp3" }
            // Extraction du nom de l'artiste
            .map { extractArtistName(it) }
            // Déduplication
            .distinct()
            // Instanciation de l'artiste
            .map { Artist(it) }
            // Récupération d'une liste à partir de l'itérable
            .toList()
    }

    override fun findAll() = artists
    override fun find(id: UUID) = artists.find { it.id == id }
}

private fun extractArtistName(file: File): String {
    return "toto"
}