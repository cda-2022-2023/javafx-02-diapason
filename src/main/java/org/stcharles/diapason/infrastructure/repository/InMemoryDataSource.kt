package org.stcharles.diapason.infrastructure.repository

import org.stcharles.diapason.domain.model.Album
import org.stcharles.diapason.domain.model.Artist
import org.stcharles.diapason.domain.model.Song
import kotlin.time.Duration

object InMemoryDataSource {
    val artists: List<Artist>
    val albums: List<Album>

    init {
        val metallica = Artist("Metallica")
        val ghost = Artist("Ghost")
        val masterOfPuppets = Album(
            metallica,
            "Master of Puppets",
            1984,
            listOf(
                Song("Battery", Duration.parse("5m 12s")),
                Song("Master of Puppets", Duration.parse("8m 35s")),
            )
        )
        val infestissumam = Album(
            ghost,
            "Infestissumam",
            2013,
            listOf(
                Song("Infestissusam", Duration.parse("1m 42s")),
                Song("Per Aspera Ad Inferi", Duration.parse("4m 9s"))
            )
        )
        artists = listOf(
            metallica,
            ghost
        )
        albums = listOf(
            masterOfPuppets,
            infestissumam
        )
    }
}